﻿using System;

namespace PrimaryMonitor.MonitorChanger
{
    public class MonitorInfo
    {
        public uint Index { get; set; }

        public string Name { get; set; }

        public string DeviceString { get; set; }

        public DisplayDeviceStateFlags StateFlags { get; set; }

        public string DeviceId { get; set; }

        public string DeviceKey { get; set; }

        public Int32 DisplayWidth { get; set; }

        public Int32 DisplayHeight { get; set; }

        public Int32 DisplayFlags { get; set; }

        public Int32 DisplayFrequency { get; set; }

        public bool IsPrimary { get; set; }

        public bool IsAttachedToDesktop { get; set; }

        public string MenuDescription
        {
            get
            {
                return "Display " + (Index + 1) + (IsAttachedToDesktop ? " - " + DisplayWidth + " x " + DisplayHeight : "");
            }
        }
    }
}