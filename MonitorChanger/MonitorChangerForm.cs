﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace PrimaryMonitor.MonitorChanger
{
    public partial class MonitorChangerForm : Form
    {
        public readonly uint MonitorIndex;
        public readonly Dictionary<uint, MonitorChangerForm> MonitorFormList;

        public MonitorChangerForm(uint index, Dictionary<uint, MonitorChangerForm> monitorFormList)
        {
            InitializeComponent();

            MonitorIndex = index;
            Text = "Monitor " + (index + 1);
            MonitorLabel.Text = Text;
            this.MonitorFormList = monitorFormList;
        }

        private readonly MonitorService monitorService = MonitorService.Instance;

        private void ActiveMonitor0_Click(object sender, EventArgs e)
        {
            ChangePrimaryMonitorAndSetFormLocation(0);
        }

        private void ActiveMonitor1_Click(object sender, EventArgs e)
        {
            ChangePrimaryMonitorAndSetFormLocation(1);
        }

        private void ActiveMonitor2_Click(object sender, EventArgs e)
        {
            ChangePrimaryMonitorAndSetFormLocation(2);
        }

        private void ActiveMonitor3_Click(object sender, EventArgs e)
        {
            ChangePrimaryMonitorAndSetFormLocation(4);
        }

        private void ChangePrimaryMonitorAndSetFormLocation(uint primary)
        {
            monitorService.SetAsPrimaryMonitor(primary);
            //SetButtonInfo(primary);

            foreach (var monitorFormList in MonitorFormList.Values)
            {
                monitorFormList?.SetButtonInfo(primary);
            }

            //var screen = monitorService.GetScreen(index);
            var screen = monitorService.GetScreen(MonitorIndex);
            SetFormLocation(screen);
        }

        public void SetButtonInfo(uint primary)
        {
            SetupButtonInfo(0, ActiveMonitor0, Size0, primary);
            SetupButtonInfo(1, ActiveMonitor1, Size1, primary);
            SetupButtonInfo(2, ActiveMonitor2, Size2, primary);
            SetupButtonInfo(3, ActiveMonitor3, Size3, primary);
        }

        private void SetFormLocation(Screen screen)
        {
            if (screen == null) return;

            var location = screen.WorkingArea.Location;

            var padding = 10;
            var x = location.X + screen.Bounds.Width - Width - padding;
            var y = location.Y + padding;
            Location = new Point(x, y);
            Show();
        }

        private void SetupButtonInfo(uint index, Button button, Label label, uint primary)
        {
            var info = monitorService.GetInfo(index);
            button.Enabled = info?.IsAttachedToDesktop ?? false;
            label.Visible = info?.IsAttachedToDesktop ?? false;
            if (info != null)
                label.Text = info.DisplayWidth + " x " + info.DisplayHeight;
            if (index == primary)
                button.Focus();
        }

        private void MonitorChangerForm_Activated(object sender, EventArgs e)
        {
            var primary = monitorService.GetPrimaryScreen();
            SetButtonInfo(primary);
            var screen = monitorService.GetScreen(MonitorIndex);
            SetFormLocation(screen);
        }

        private void MonitorChangerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            //this.Hide();
        }
    }
}