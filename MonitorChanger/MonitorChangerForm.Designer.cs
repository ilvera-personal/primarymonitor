﻿
namespace PrimaryMonitor.MonitorChanger
{
    partial class MonitorChangerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MonitorChangerForm));
            this.ActiveMonitor0 = new System.Windows.Forms.Button();
            this.ActiveMonitor1 = new System.Windows.Forms.Button();
            this.ActiveMonitor2 = new System.Windows.Forms.Button();
            this.ActiveMonitor3 = new System.Windows.Forms.Button();
            this.Size0 = new System.Windows.Forms.Label();
            this.Size1 = new System.Windows.Forms.Label();
            this.Size2 = new System.Windows.Forms.Label();
            this.Size3 = new System.Windows.Forms.Label();
            this.MonitorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ActiveMonitor0
            // 
            this.ActiveMonitor0.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActiveMonitor0.Location = new System.Drawing.Point(10, 40);
            this.ActiveMonitor0.Name = "ActiveMonitor0";
            this.ActiveMonitor0.Size = new System.Drawing.Size(75, 44);
            this.ActiveMonitor0.TabIndex = 0;
            this.ActiveMonitor0.Text = "1";
            this.ActiveMonitor0.UseVisualStyleBackColor = true;
            this.ActiveMonitor0.Click += new System.EventHandler(this.ActiveMonitor0_Click);
            // 
            // ActiveMonitor1
            // 
            this.ActiveMonitor1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActiveMonitor1.Location = new System.Drawing.Point(90, 40);
            this.ActiveMonitor1.Name = "ActiveMonitor1";
            this.ActiveMonitor1.Size = new System.Drawing.Size(75, 44);
            this.ActiveMonitor1.TabIndex = 1;
            this.ActiveMonitor1.Text = "2";
            this.ActiveMonitor1.UseVisualStyleBackColor = true;
            this.ActiveMonitor1.Click += new System.EventHandler(this.ActiveMonitor1_Click);
            // 
            // ActiveMonitor2
            // 
            this.ActiveMonitor2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActiveMonitor2.Location = new System.Drawing.Point(171, 40);
            this.ActiveMonitor2.Name = "ActiveMonitor2";
            this.ActiveMonitor2.Size = new System.Drawing.Size(75, 44);
            this.ActiveMonitor2.TabIndex = 2;
            this.ActiveMonitor2.Text = "3";
            this.ActiveMonitor2.UseVisualStyleBackColor = true;
            this.ActiveMonitor2.Click += new System.EventHandler(this.ActiveMonitor2_Click);
            // 
            // ActiveMonitor3
            // 
            this.ActiveMonitor3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ActiveMonitor3.Location = new System.Drawing.Point(252, 40);
            this.ActiveMonitor3.Name = "ActiveMonitor3";
            this.ActiveMonitor3.Size = new System.Drawing.Size(75, 44);
            this.ActiveMonitor3.TabIndex = 4;
            this.ActiveMonitor3.Text = "4";
            this.ActiveMonitor3.UseVisualStyleBackColor = true;
            this.ActiveMonitor3.Click += new System.EventHandler(this.ActiveMonitor3_Click);
            // 
            // Size0
            // 
            this.Size0.Location = new System.Drawing.Point(10, 87);
            this.Size0.Name = "Size0";
            this.Size0.Size = new System.Drawing.Size(75, 19);
            this.Size0.TabIndex = 5;
            this.Size0.Text = "Size";
            this.Size0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Size1
            // 
            this.Size1.Location = new System.Drawing.Point(91, 87);
            this.Size1.Name = "Size1";
            this.Size1.Size = new System.Drawing.Size(75, 19);
            this.Size1.TabIndex = 6;
            this.Size1.Text = "Size";
            this.Size1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Size2
            // 
            this.Size2.Location = new System.Drawing.Point(168, 87);
            this.Size2.Name = "Size2";
            this.Size2.Size = new System.Drawing.Size(75, 19);
            this.Size2.TabIndex = 7;
            this.Size2.Text = "Size";
            this.Size2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Size3
            // 
            this.Size3.Location = new System.Drawing.Point(249, 87);
            this.Size3.Name = "Size3";
            this.Size3.Size = new System.Drawing.Size(75, 19);
            this.Size3.TabIndex = 8;
            this.Size3.Text = "Size";
            this.Size3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MonitorLabel
            // 
            this.MonitorLabel.AutoSize = true;
            this.MonitorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MonitorLabel.Location = new System.Drawing.Point(6, 9);
            this.MonitorLabel.Name = "MonitorLabel";
            this.MonitorLabel.Size = new System.Drawing.Size(80, 24);
            this.MonitorLabel.TabIndex = 9;
            this.MonitorLabel.Text = "Monitor";
            // 
            // MonitorChangerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 115);
            this.Controls.Add(this.MonitorLabel);
            this.Controls.Add(this.Size3);
            this.Controls.Add(this.Size2);
            this.Controls.Add(this.Size1);
            this.Controls.Add(this.Size0);
            this.Controls.Add(this.ActiveMonitor3);
            this.Controls.Add(this.ActiveMonitor2);
            this.Controls.Add(this.ActiveMonitor1);
            this.Controls.Add(this.ActiveMonitor0);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MonitorChangerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Primary monitor";
            this.Activated += new System.EventHandler(this.MonitorChangerForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MonitorChangerForm_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ActiveMonitor0;
        private System.Windows.Forms.Button ActiveMonitor1;
        private System.Windows.Forms.Button ActiveMonitor2;
        private System.Windows.Forms.Button ActiveMonitor3;
        private System.Windows.Forms.Label Size0;
        private System.Windows.Forms.Label Size1;
        private System.Windows.Forms.Label Size2;
        private System.Windows.Forms.Label Size3;
        private System.Windows.Forms.Label MonitorLabel;
    }
}

