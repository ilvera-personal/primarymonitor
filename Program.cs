﻿using System;
using System.Threading;
using System.Windows.Forms;

namespace PrimaryMonitor
{
    internal static class Program
    {
        private static Mutex mutex = null;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            string appName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

            // ------------------------------------------------------------
            // Istansa unica
            bool createdNew;
            mutex = new Mutex(true, appName, out createdNew);
            if (!createdNew)
            {
                return;
            }
            // ------------------------------------------------------------

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Startup startup = new Startup();
            startup.AllowShowDisplay = false;

            Application.Run(startup);
        }
    }
}