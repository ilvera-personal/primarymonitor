﻿using PrimaryMonitor.MonitorChanger;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;

namespace PrimaryMonitor
{
    public partial class Startup : Form
    {
        public Startup()
        {
            InitializeComponent();

            InitializeData();

            CreateIconMenuStructure();
        }

        private readonly MonitorService monitorService = MonitorService.Instance;
        private readonly NotifyIcon notifyIcon = new NotifyIcon();
        private Dictionary<uint, MonitorChangerForm> monitorFormList;
        private readonly List<MenuItem> monitorMenu = new List<MenuItem>();

        private void InitializeData()
        {
            var max = Screen.AllScreens.Length;
            monitorFormList = new Dictionary<uint, MonitorChangerForm>(max);
        }

        public bool AllowShowDisplay { get; set; } = false;

        /// <summary>
        /// Nasconde la from all'avvio
        /// </summary>
        /// <param name="value"></param>
        protected override void SetVisibleCore(bool value)
        {
            base.SetVisibleCore(AllowShowDisplay ? value : AllowShowDisplay);
        }

        private void MenuItemOpen_Click(object sender, System.EventArgs e)
        {
            for (uint i = 0; i < Screen.AllScreens.Length; i++)
            {
                var info = monitorService.GetInfo((uint)i);
                if (info.IsAttachedToDesktop)
                {
                    monitorFormList.TryGetValue(i, out MonitorChangerForm monitorForm);
                    // ...creo/apro la finestra
                    if (monitorForm == null)
                    {
                        monitorForm = new MonitorChangerForm(i, monitorFormList);
                        monitorFormList.Add(i, monitorForm);
                    }
                    if (!monitorForm.Visible)
                    {
                        monitorForm.Show();
                    }
                }
                else if (monitorFormList[i] != null)
                {
                    // ...la distruggo
                    monitorFormList[i].Close();
                    monitorFormList[i] = null;
                }
            }
        }

        private void NotifyIconOpen_Click(object sender, System.EventArgs e)
        {
            if (e is System.Windows.Forms.MouseEventArgs arg)
                if (arg.Button == MouseButtons.Right) return;

            for (uint i = 0; i < Screen.AllScreens.Length; i++)
            {
                var info = monitorService.GetInfo(i);
                monitorFormList.TryGetValue(i, out MonitorChangerForm monitorForm);

                if (info.IsAttachedToDesktop)
                {
                    // ...creo/apro la finestra
                    if (monitorForm == null)
                    {
                        monitorForm = new MonitorChangerForm(i, monitorFormList);
                        monitorFormList.Add(i, monitorForm);
                    }
                    if (!monitorForm.Visible)
                    {
                        monitorForm.Show();
                    }
                    else
                    {
                        // ...o la chiudo
                        monitorForm.Hide();
                    }
                }
                else if (monitorFormList[i] != null)
                {
                    // ...la distruggo
                    monitorForm.Close();
                    monitorForm = null;
                }
            }
        }

        private void MenuItemRefresh_Click(object sender, System.EventArgs e)
        {
            var primary = monitorService.GetPrimaryScreen();

            uint index = 0;
            monitorMenu.ForEach(x =>
            {
                var info = monitorService.GetInfo(index);

                x.Enabled = info.IsAttachedToDesktop;
                x.Text = info.MenuDescription;
                x.Checked = index == primary;
                index++;
            });
        }

        private void MenuItemRunOnStartup_Click(object sender, System.EventArgs e)
        {
            var item = (MenuItem)sender;
            if (item.Checked)
            {
                RunOnStartup.Startup.RemoveFromStartup();
            }
            else
            {
                RunOnStartup.Startup.RunOnStartup();
            }
            item.Checked = !item.Checked;
        }

        private void MenuItemDisplay_Click(object sender, System.EventArgs e)
        {
            var item = (MenuItem)sender;
            var info = (MonitorInfo)item.Tag;
            var primary = monitorService.GetPrimaryScreen();
            monitorMenu[(int)primary].Checked = false;

            item.Checked = true;
            monitorService.SetAsPrimaryMonitor(info.Index);
            for (uint i = 0; i < Screen.AllScreens.Length; i++)
            {
                monitorFormList.TryGetValue(i, out MonitorChangerForm monitorForm);
                monitorForm?.Hide();
            }
        }

        private void MenuItemClose_Click(object sender, System.EventArgs e)
        {
            notifyIcon.Visible = false;
            //if (MonitorForm != null) MonitorForm.Close();
            //this.Close();

            Application.Exit();
            Environment.Exit(0);
        }

        public void CreateIconMenuStructure()
        {
            var monitorInfo = monitorService.GetInfo();
            var primary = monitorService.GetPrimaryScreen();

            ContextMenu contextMenu = new ContextMenu();

            // OPEN
            MenuItem item = new MenuItem()
            {
                DefaultItem = true,
                Text = "Open",
            };
            item.Click += new EventHandler(this.MenuItemOpen_Click);
            contextMenu.MenuItems.Add(item);
            // REFRESH
            item = new MenuItem()
            {
                Text = "Refresh",
            };
            item.Click += new EventHandler(this.MenuItemRefresh_Click);
            contextMenu.MenuItems.Add(item);
            // --------------
            contextMenu.MenuItems.Add("-");
            // RUN ON STARTUP
            item = new MenuItem()
            {
                Text = "Run on startup",
                Checked = RunOnStartup.Startup.IsInStartup()
            };
            item.Click += new EventHandler(this.MenuItemRunOnStartup_Click);
            contextMenu.MenuItems.Add(item);
            // --------------
            contextMenu.MenuItems.Add("-");
            // DISPLAY
            uint index = 0;
            monitorInfo.ForEach(x =>
            {
                item = new MenuItem()
                {
                    Enabled = x.IsAttachedToDesktop,
                    Text = x.MenuDescription,
                    Tag = x
                };
                item.Click += new EventHandler(this.MenuItemDisplay_Click);
                item.Checked = index == primary;
                contextMenu.MenuItems.Add(item);
                monitorMenu.Add(item);
                index++;
            });
            // --------------
            contextMenu.MenuItems.Add("-");
            // EXIT
            item = new MenuItem()
            {
                Text = "Exit",
            };
            item.Click += new EventHandler(this.MenuItemClose_Click);
            contextMenu.MenuItems.Add(item);

            Icon appIcon = Icon.ExtractAssociatedIcon(Assembly.GetExecutingAssembly().Location);

            // Set properties of NotifyIcon component.
            notifyIcon.Visible = true;
            notifyIcon.Icon = appIcon;
            notifyIcon.Text = "Right-click me!";
            notifyIcon.Click += new EventHandler(NotifyIconOpen_Click);

            //
            notifyIcon.ContextMenu = contextMenu;
        }
    }
}